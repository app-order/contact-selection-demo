//
//  ViewController.h
//  ContactSelectionDemo
//
//  Created by Anthony Miller on 9/12/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <AddressBookUI/AddressBookUI.h>

#import "AOContactUpdateDelegate.h"

@interface ViewController : UIViewController <ABPeoplePickerNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate, AOContactUpdateDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, atomic) NSMutableArray *selectedContacts;

@end
