//
//  AOContactUpdateDelegate.h
//  ContactSelectionDemo
//
//  Created by Anthony Miller on 9/15/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AOContact.h"

/**
 *  This delegate allows a controller to react to information being updated on an `AOContact`.
 */
@protocol AOContactUpdateDelegate <NSObject>

/**
 *  This delegate method is called when a contact is updated with any new information after its initalization.
 *
 *  @param contact The `AOContact` being updated.
 */
- (void)contactDidUpdate:(AOContact *)contact;

@end
