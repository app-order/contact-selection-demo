//
//  AOContact.h
//  ContactSelectionDemo
//
//  Created by Anthony Miller on 9/12/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>
#import <UIKit/UIKit.h>

@protocol AOContactUpdateDelegate;

/**
 *  This class defines a contact from an address book
 */
@interface AOContact : NSObject <UIActionSheetDelegate>

/**
 *  Initalizes a new `AOContactPickerContact` with values set from an `ABPerson` (represented by an instance of the ABRecordRef opaque type).
 *
 *  @param person The `ABPerson` to create a contact item for.
 *
 *  @return The `AOContactPickerContact` with the values from the given `ABPerson`.
 */
- (instancetype)initWithABPerson:(ABRecordRef)person;

/**
 *  The delegate to be notified of updates to the contact.
 */
@property (nonatomic, weak) id <AOContactUpdateDelegate> delegate;

/**
 *  The `ABPerson` (represented by an instance of the ABRecordRef opaque type) for the contact.
 */
@property (assign, nonatomic) ABRecordRef personRecord;

/**
 *  The name to display for the contact
 */
@property (strong, nonatomic, readonly) NSString *displayName;

/**
 *  The contact info to display for the contact
 */
@property (strong, nonatomic, readonly) NSString *contactInfo;

/**
 * An `NSArray`of email addresses for the contact
 */
- (NSArray *)emailAddresses;

/**
 *  A contact image to display
 */
@property (nonatomic) UIImage *contactImage;

@end
