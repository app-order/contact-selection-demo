//
//  ViewController.m
//  ContactSelectionDemo
//
//  Created by Anthony Miller on 9/12/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "ViewController.h"

#import "AOContact.h"

@implementation ViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
  self.selectedContacts = [NSMutableArray array];
}

- (IBAction)addContactButtonPressed:(id)sender
{
  ABPeoplePickerNavigationController *peoplePicker = [[ABPeoplePickerNavigationController alloc] init];
  peoplePicker.peoplePickerDelegate = self;
  
  [self presentViewController:peoplePicker animated:YES completion:nil];
}

#pragma mark - ABPeoplePickerNavigationControllerDelegate

- (void)peoplePickerNavigationControllerDidCancel:
(ABPeoplePickerNavigationController *)peoplePicker
{
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
                         didSelectPerson:(ABRecordRef)person
{
  [self handlePeoplePickerSelectionWithPerson:person];
}

- (BOOL)peoplePickerNavigationController:(ABPeoplePickerNavigationController *)peoplePicker
      shouldContinueAfterSelectingPerson:(ABRecordRef)person
{
  [self handlePeoplePickerSelectionWithPerson:person];
  
  return NO;
}

- (void)handlePeoplePickerSelectionWithPerson:(ABRecordRef)person
{
  [self addPersonToContactList:person];
  [self.tableView reloadData];
  [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)addPersonToContactList:(ABRecordRef)person
{
  AOContact *contact = [[AOContact alloc] initWithABPerson:person];
  
  if ([contact emailAddresses].count) {
    contact.delegate = self;
    
    [self.selectedContacts addObject:contact];
    [self.tableView reloadData];
  } else {
    [self showNoEmailAlert];
    
  }
}

- (void)showNoEmailAlert
{
  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"No E-mail", nil)
                                                      message:NSLocalizedString(@"The selected contact has no e-mail address. Please select a contact with an e-mail address to send an invitiation.", nil)
                                                     delegate:nil
                                            cancelButtonTitle:nil
                                            otherButtonTitles:NSLocalizedString(@"Ok", nil), nil];
  [alertView show];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return self.selectedContacts.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactCell"];
  if (!cell) {
    cell = [self newCell];
  }
  
  AOContact *contact = [self contactForIndexPath:indexPath];
  [self configureCell:cell withContact:contact];
  
  return cell;
}

- (UITableViewCell *)newCell
{
  return [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"ContactCell"];
}

- (AOContact *)contactForIndexPath:(NSIndexPath *)indexPath
{
  return self.selectedContacts[indexPath.row];
}

- (void)configureCell:(UITableViewCell *)cell withContact:(AOContact *)contact
{
  [self setTitleForCell:cell withContact:contact];
  [self setSubtitleForCell:cell withContact:contact];
  [self setImageForCell:cell withContact:contact];
}

- (void)setTitleForCell:(UITableViewCell *)cell withContact:(AOContact *)contact
{
  cell.textLabel.text = contact.displayName;
}

- (void)setSubtitleForCell:(UITableViewCell *)cell withContact:(AOContact *)contact
{
  cell.detailTextLabel.text = nil;
  
  if (contact.displayName.length) {
    cell.detailTextLabel.text = contact.contactInfo;
  }
}

- (void)setImageForCell:(UITableViewCell *)cell withContact:(AOContact *)contact
{
  cell.imageView.image = nil;
  
  if (contact.contactImage) {
    cell.imageView.image = contact.contactImage;
  }
}

#pragma mark - AOContactUpdateDelegate

- (void)contactDidUpdate:(AOContact *)contact
{
  [self reloadRowForContact:contact];
}

- (void)reloadRowForContact:(AOContact *)contact
{
  NSIndexPath *indexPath = [self indexPathForContact:contact];
  [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (NSIndexPath *)indexPathForContact:(AOContact *)contact
{
  NSUInteger index = [self.selectedContacts indexOfObject:contact];
  return [NSIndexPath indexPathForRow:index inSection:0];
}

@end
