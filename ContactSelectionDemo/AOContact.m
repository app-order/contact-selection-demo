//
//  AOContact.m
//  ContactSelectionDemo
//
//  Created by Anthony Miller on 9/12/14.
//  Copyright (c) 2014 App-Order. All rights reserved.
//

#import "AOContact.h"

#import "AOContactUpdateDelegate.h"

@interface AOContact ()

@property (strong, nonatomic) NSString *selectedEmailAddress;

@end

@implementation AOContact

- (instancetype)initWithABPerson:(ABRecordRef)person
{
  self = [super init];
  if (self) {
    CFRetain(person);
    self.personRecord = person;
    [self setContactInfo];
  }
  return self;
}

- (void)setContactInfo
{
  if ([self emailAddresses].count == 1) {
    self.selectedEmailAddress = [self emailAddresses][0];
    
  } else if ([self emailAddresses].count > 1) {
    [self showSelectEmailActionSheet];
  }
}

- (NSArray *)emailAddresses
{
  ABMultiValueRef emailsRef = ABRecordCopyValue(self.personRecord, kABPersonEmailProperty);
  
  NSArray *emailArray = (__bridge_transfer NSArray *)ABMultiValueCopyArrayOfAllValues(emailsRef);
  return emailArray;
}

- (void)showSelectEmailActionSheet
{
  UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select Email", nil) delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
  
  for (NSString *email in [self emailAddresses]) {
    [actionSheet addButtonWithTitle:email];
    
  }
  UIWindow* window = [[[UIApplication sharedApplication] delegate] window];
  [actionSheet showInView:window];
}

#pragma mark - UIActionSheetDelegate 

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
  NSString *selectedEmailAddress = [actionSheet buttonTitleAtIndex:buttonIndex];
  self.selectedEmailAddress = selectedEmailAddress;
  
  [self.delegate contactDidUpdate:self];
}

#pragma mark - Custom Accessors

- (NSString *)displayName
{
  return [NSString stringWithFormat:@"%@%@%@",
          [self firstName],
          [self nameSeperator],
          [self lastName]];
}

- (NSString *)firstName
{
  NSString *name = (__bridge_transfer NSString *)ABRecordCopyValue(self.personRecord,
                                                                   kABPersonFirstNameProperty);
  return name.length ? name : @"";
}

- (NSString *)lastName
{
  NSString *name = (__bridge_transfer NSString *)ABRecordCopyValue(self.personRecord,
                                                                   kABPersonLastNameProperty);
  return name.length ? name : @"";
}

- (NSString *)nameSeperator
{
  if ([self firstName].length) {
    return @" ";
  }
  else {
    return @"";
  }
}

- (NSString *)contactInfo
{
  return self.selectedEmailAddress;
}

- (void)dealloc
{
  CFRelease(self.personRecord);
}

@end
